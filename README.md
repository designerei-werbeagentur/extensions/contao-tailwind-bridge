# Contao-Tailwind-Bridge

## Function & usage

This extension for Contao Open Source CMS handles as bridge between contao and tailwind for generating tailwind classes based on defined configuration values

```yml
contao_tailwind:
  safelist_dir: 'var/tailwind/safelist'
  safelist_filename: 'safelist'
  screens: []
```
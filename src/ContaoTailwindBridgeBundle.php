<?php

declare(strict_types=1);

namespace designerei\ContaoTailwindBridgeBundle;

use designerei\ContaoTailwindBridgeBundle\DependencyInjection\ContaoTailwindBridgeExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class ContaoTailwindBridgeBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new ContaoTailwindBridgeExtension();
    }
}

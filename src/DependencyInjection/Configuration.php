<?php

namespace designerei\ContaoTailwindBridgeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('designerei_contao_tailwind_bridge');

        // Keep compatibility with symfony/config < 4.2
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('designerei_contao_tailwind_bridge');
        }

        $rootNode
            ->children()
                ->scalarNode('safelist_dir')
                    ->defaultValue('var/tailwind/safelist')
                ->end()
                ->scalarNode('safelist_filename')
                    ->defaultValue('safelist')
                ->end()
                ->arrayNode('screens')
                    ->scalarPrototype()->end()
                    ->defaultValue([])
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}

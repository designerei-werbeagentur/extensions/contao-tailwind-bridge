<?php

declare(strict_types=1);

namespace designerei\ContaoTailwindBridgeBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ContaoTailwindBridgeExtension extends Extension
{
    public function getAlias(): string
    {
        return 'contao_tailwind';
    }

    public function load(array $config, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $config);

        $container->setParameter('contao_tailwind.screens', $config['screens']);
        $container->setParameter('contao_tailwind.safelist_dir', $config['safelist_dir']);
        $container->setParameter('contao_tailwind.safelist_filename', $config['safelist_filename']);
    }
}
